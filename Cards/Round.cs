﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cards
{
    public static class Round
    {
        private static List<Hand> _hands = new List<Hand>();

        public static void Start(IEnumerable<Hand> hands, int deal)
        {
            _hands.AddRange(hands);
            var deck = new Deck(_hands);
            var Player = _hands[0];

            deck.Shuffle();
            deck.Deal(deal);

            foreach (var hand in hands)
            {
                hand.SortCards();
            }

            Console.WriteLine("Twoje karty:\n" + Player.ShowCards());
        }

        public static void ShowUp()
        {
            for (int i = 1; i < _hands.Count; i++)
            {
                Console.WriteLine($"Przeciwnik { i }:\n { _hands[i].ShowCards() }");
            }
        }
    }
}
