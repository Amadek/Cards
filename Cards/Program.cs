﻿using System;
using System.Collections.Generic;

namespace Cards
{
    class Program
    {
        static void Main(string[] args)
        {
            Round.Start(new Hand[] { new Hand(), new Hand(), new Hand() }, 5);
            Round.ShowUp();

            Console.ReadKey();
        }
    }
}
