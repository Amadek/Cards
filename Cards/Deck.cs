﻿using System;
using System.Collections.Generic;

namespace Cards
{
    public class Deck : Hand
    {
        private List<Hand> _hands = new List<Hand>();

        public Deck(IEnumerable<Hand> hands)
        {
            _hands.AddRange(hands);
            foreach (var color in Card.COLORS.Keys)
            {
                foreach (var shape in Card.SHAPES.Keys)
                {
                    AddCard(new Card(shape, color));
                }
            }
        }
        
        public void Shuffle()
        {
            var bufor = new List<Card>();

            Random rnd = new Random();
            int i;

            while (_Cards.Count != 0)
            {
                i = rnd.Next(0, _Cards.Count - 1);
                bufor.Add(_Cards[i]);
                _Cards.Remove(_Cards[i]);
            }

            _Cards = bufor;
        }

        public void Deal(int cardsPerPlayer)
        {
            for (int i = 0; i < cardsPerPlayer; i++) // Amount cards per player.
            {
                for (int j = 0; j < _hands.Count; j++)
                {
                    GiveCard(_Cards[0], _hands[j]);
                }
            }
        }
    }
}
