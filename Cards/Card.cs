﻿using System;
using System.Collections.Generic;

namespace Cards
{
    public class Card
    {
        public string Color { get; }
        public string Shape { get; }
        public int Value { get; }

        public static Dictionary<string, int> COLORS = new Dictionary<string, int>()
        {
            { "♥", 1 },
            { "♦", 2 },
            { "♠", 3 },
            { "♣", 4 }
        };

        public static Dictionary<string, int> SHAPES = new Dictionary<string, int>()
        {
            { "A", 1 }, { "9", 10 },
            { "1", 2 }, { "10", 11 },
            { "2", 3 }, { "J", 12 },
            { "3", 4 }, { "D", 13 },
            { "4", 5 }, { "K", 14 },
            { "5", 6 },
            { "6", 7 },
            { "7", 8 },
            { "8", 9 },
        };

        public Card(string shape, string color)
        {
            Color = color;
            Shape = shape;
            Value = COLORS[Color] * 100 + SHAPES[Shape];
        }

        public string Show()
        {
            return $"{ Shape }{ Color }";
        }
    }
}
