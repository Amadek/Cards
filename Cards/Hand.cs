﻿using System;
using System.Collections.Generic;

namespace Cards
{
    public class Hand
    {
        protected List<Card> _Cards = new List<Card>();

        public Card this[string val]
        {
            get
            {
                foreach (var card in _Cards)
                {
                    if (val == card.Show())
                    {
                        return card;
                    }
                }
                return null;
            }
        }

        public void SortCards()
        {
            _Cards.Sort(delegate (Card x, Card y)
            {
                return x.Value.CompareTo(y.Value);
            });
        }

        public void AddCard(Card card)
        {
            _Cards.Add(card);
        }

        public void GiveCard(Card card, Hand otherHand)
        {
            if (_Cards.Contains(card))
            {
                _Cards.Remove(card);
                otherHand.AddCard(card);
            }
            else Console.WriteLine("ERROR: First hand hasn\'t got this card");
        }

        public string ShowCards()
        {
            string s = "";
            foreach (var item in _Cards)
            {
                s += item.Show() + "\t";
            }
            return s;
        }
    }
}
